# kali's fork

* As of today (Dec 5 2023), I'm not able to make invitations work with `dev/backend` HEAD (dev branch).
* I'm pushing a branch that uses the admin password directly to create users: https://gitlab.com/kalikaneko/colmena-backend/-/commits/bug/use-superadmin-pass
* I've changed docker compose to fetch that image (from kali's `dev` tag in the registry), until we can debug this further.
* I'll try to keep this branch rebased but I can't promise anything.

# Changing the domain name

- Change the extra-hosts passed to caddy
- Change the sites fixture (this needs to flush postgres + reload backend), *or* manually edit sites entries.
