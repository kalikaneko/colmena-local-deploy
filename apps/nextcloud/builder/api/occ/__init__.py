import re
import subprocess

from logzero import logger
from settings.config import APP_PASSWORD_CREATION_TIMEOUT

# Subprocess timeout in seconds
TIMEOUT = APP_PASSWORD_CREATION_TIMEOUT


def create_app_password(username, password):
    nc_username = username
    nc_password = password
    script_path = "/colmena_occ.sh"
    command_name = "create_app_password"

    command = [
        script_path,
        command_name,
        nc_username,
        nc_password,
    ]

    result = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=False,
        text=True,
        timeout=TIMEOUT,
    )

    if result.returncode == 0:
        # SSH command succeeded
        stdout = parse_stdout(result.stdout)
        logger.info(f"[create_app_password] Successfully created app password.")
        return stdout
    else:
        # SSH command failed
        logger.error(
            f"[create_app_password] There was an error while creating an app password stdout={result.stdout} error_code={result.returncode}."
        )
        raise AppPasswordException(result.stdout)


def parse_stdout(stdout):
    password_pattern = r"app password:\n(.*?)\n"
    match = re.search(password_pattern, stdout)
    # Check if a match was found
    if match:
        # Extract the password from the matched group (group 1)
        password = match.group(1)
        return password
    else:
        raise AppPasswordException(stdout)


class AppPasswordException(Exception):
    """Raised when the app password could not be created.

    Attributes:
        stdout -- output string where the password should be extracted from
        message -- explanation of the error
    """

    def __init__(self, stdout):
        self.stdout = stdout
        self.message = self.build_message()

        return super().__init__(self.message)

    def build_message(self):
        # Add more app password errors cases here.
        if self.stdout_contains(self.stdout, "User does not exist"):
            return "The given username does not exist."
        return "Unknown error"

    def get_message(self):
        return self.message

    def stdout_contains(self, stdout, substring):
        return stdout.lower().find(substring.lower()) != -1
