from apiflask import APIFlask, Schema, abort
from apiflask.fields import Integer, String
from apiflask.validators import OneOf
from flask import jsonify, request
from occ import create_app_password, AppPasswordException
from logger import setup_logger
from logzero import logger
from settings.config import OPENAPI_SCHEMA_VERSION

app = APIFlask(__name__, title="Nextcloud API wrapper", version=OPENAPI_SCHEMA_VERSION)
app.config["DESCRIPTION"] = "The openapi spec for the Nextcloud API wrapper"

setup_logger()


class HealthcheckOut(Schema):
    status = String()


@app.get("/api/healthcheck")
@app.output(HealthcheckOut, status_code=200)
@app.doc(
    summary="Return a healthcheck status",
    operation_id="get_healthcheck",
    tags=["healthcheck"],
)
def healthcheck():
    return {"status": "OK"}


class AppPasswordIn(Schema):
    username = String(required=True)
    password = String(required=True)


class AppPasswordOut(Schema):
    app_password = String()


@app.post("/api/occ/app_password")
@app.input(AppPasswordIn, location="json")
@app.output(AppPasswordOut, status_code=201)
@app.doc(
    summary="Return a new app password for a user",
    operation_id="create_app_password",
    tags=["app_password"],
)
def app_password(json_data):
    # Add a guard mechanism to prevent unauthenticated requests.
    # Provide an API_KEY allowed list from environment, for example.
    # TODO: decrypt and decode body params
    #
    try:
        app_password = create_app_password(json_data["username"], json_data["password"])
        # TODO: Encrypt returned values
        return {"app_password": app_password}

    except AppPasswordException as e:
        logger.error(f"[create_app_password] Exception raised: {e.get_message()}")
        return abort(400, e.get_message())
