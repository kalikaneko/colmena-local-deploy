from datetime import datetime
from settings.config import LOGS_DIR
from logzero import logger, logfile


def execution_timestamp():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-00")


def build_logfile_name():
    return f"{LOGS_DIR}/{execution_timestamp()}.log"


def setup_logger():
    logfile(build_logfile_name(), maxBytes=1e6, backupCount=3)
