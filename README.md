# Colmena Local Deploy

Deploy a local colmena environment (for demo purposes, mostly)[^0]

[^0]: I don't intend to maintain this repo, looking forward. The sole purpose of
this repo is to facilitate early testing and QA with the colmena app
environment.

**Disclaimer**: The code contained in this repo is provided as-is, and I'm not responsible of
any data loss if you're following these instructions. You should check the
official documentation of the project. 

[[_TOC_]]

# What to expect

After following this tutorial, you should be able to have access to colmena
frontend, backend and supporting instances (nextcloud, test mail server) to
replicate the official QA environments, in your own infrastructure.

Beware that we will not worry about securing the test instance! (no encryption
is enabled at this point). So please make sure that your test users know they
should not enter any valuable information.

## What is expected from you

You should have some basic familiarity (at least working concepts) with
containers and orchestration. You also need to add custom DNS records for your
machine or local network.

## What you should not expect

This is *not* the way to have a working environment for development. This might
fit your purposes if you change some things, but you're better off perusing
camba's official devops repo, or the backend or frontend documentation[^1].

[^1]: That said, have a look at `make start-local-dev` if you're interested in
    developing, it exposes the internal ports in localhost too.

This is *not* a way to deploy a *secure* colmena environment in production. However, 
you might find it useful towards that purpose, but you will need to put a little
bit of work from your side.

Again, this is just a way of having more people testing the current state of
the system (not necessarily developers), and transition into more stable
solutions later on.

## Pre-requisites

Make sure you have these dependencies installed in your host machine:

- [docker](https://docs.docker.com/engine/install/docker) and [docker compose](https://docs.docker.com/compose/install/) installed in your system
- [tilt](https://docs.tilt.dev/install.html)
- git

Additionally, you will need at least 2GB of memory in the build environment (the same server where you will be deploying the service, for now).

I'm generally assuming a GNU/linux environment. In OSX or MS windows, [YMMV](https://en.wiktionary.org/wiki/your_mileage_may_vary).

## Convenience script

Instead of following all the steps, the following script *might* work for you. It already tries to install all the dependencies, etc.

This script assumes a debian system (tested with debian 12), and it's expected to be run from a non-root user that already has `sudo` permissions.

```
curl https://gitlab.com/kalikaneko/colmena-local-deploy/-/raw/main/install.sh?ref_type=heads | bash
```

The only inconvenient is that this will not show tilt if you're running this in
a VPS - at some point, you might want to interrupt it and then launch Tilt again. Probably you want to also open the port at this point:

```
^C^C
sudo ufw allow 9999
make start TILT_HOST=1.2.3.4 TILT_PORT=9999
```

where `1.2.3.4` is the public IP for your server. 

# Step-by-step Guide

## 00: Clone this repo

Note: respect the `deploy` clone name, a few names in the docker compose are hardcoded for this.

```
git clone https://gitlab.org/kalikaneko/colmena-local-deploy.git deploy
cd deploy
```

## 01: Add local name entries

The easiest, if you're testing in your own machine, is to add entries in your `/etc/hosts` (or equivalent in windows):


```
# add to /etc/hosts:
127.0.0.1 backend.colmena.local
127.0.0.1 nextcloud.colmena.local
127.0.0.1 mail.colmena.local
127.0.0.1 colmena.local
```

If you plan to install this in a different server, add the server IP instead of
`127.0.0.1`. After verifying everything works, you can add it to your own `DNS`
server. [^2]

[^2]: For automated TLS certificates with LetsEncrypt (not yet implemented), you
will want to use a FQDN for a *real* DNS domain that you "own". For the test
stage, we're simply using private DNS names.


## 02: Inspect the provided .env files

In an ideal world, I would give you an `.env.example` file and you would copy
and customize it, ensuring that neither you or me commit working credentials to
the repo. However, I'm still debugging the whole setup, so I've decided to ship a single
env file.

At the very least, you might want to change the superadmin credentials.

Do read and edit `.env` and `.env.backend` (some unification might be in order here).

## 03: Have a look at the docker-compose.yaml file

It's not very large.

### At a glance: components of the system

You should get yourself familiar with the different containers

* **caddy**: this is the entrypoint for my setup. it's a http server with some
    nice abilities (like automatic TLS certificates, more about this later). It's
    configured to listen on some preconfigured domain names (the ones we did
    setup in step `01`). Caddy is also configured to serve the assets built from
    the frontend project. The colmena app is a Progressive Web Application (PWA); this means
    that there's no real filesystem to serve, only a bunch of javascript and css files that
    do a lot of magic in the browser. Oh, and since there's no proper html or server-side rendering (yet), 
    all this is poorly optimized for SEO. Which might be an advantage. Anyway, the frontend
    will make calls to the backend.

* **backend**: the django application for the backstage admin, where only staff
    has access. This also serves the API that the frontend PWA will need access
    to.

* **postgres**: a database for the backend. A cool, robust one.

* **nextcloud**: everyone knows what nextcloud is, isn't it? we need to ship some extra bits on top
    of the official image, because of *reasons*. Nextcloud is kind of picky about some things, so you
    need to get several configs right. Some of these configs are only applied the first time that you
    install nextcloud, so if you mess up you might want to just delete the
    permanent storage (under the `data/nextcloud` folder) and start again. You
    can do that with `make clean-all` (it will also flush other databases).

* **nextcloud-housekeeping**: it's executed after the main nextcloud image is up. Right now it's only used
   to install `spree` (another name for the nextcloud `Talk` app).

* **mail**:  when you deploy this in the real world (tm) you will have to
    configure a real email system. until that day, we're using a mock system to simulate all those wasted
    inboxes. It will be accessible using `mail.colmena.local` - caddy will route that to a cool app called `mailcrab`. Again,
    this is only to be able to quickly see any mails that the backend sends - but there's zero privacy with this system!

### Tools of the trade

Specially if you will be in charge of getting this test deployment updated, or adapting it to the needs of your organization:

- Get yourself familiar with Tilt.
- Also, you might want to install [ctop](https://github.com/bcicen/ctop),
  specially if you're in a remote debugging session. It will allow you to
  quickly see stats, stop containers, and get a shell inside them.

## 04: Build and run containers

```
make start
```

This will start `tilt` on the configured port (`localhost:9998`). Under the
hood, tilt "just" builds and runs the services declared in the docker compose
file.


### (optional) Run tilt in a different host/port

If you're running on a different machine (e.g., on a remote server you access via ssh)
you might want to change the interface tilt will listen on:


```
make start TILT_HOST=192.168.10.100 TILT_PORT=4444
```

Having it listen on all interfaces is discouraged.

### Troubleshooting tips

Ideally everything works, but that's rarely the case. please open an issue if
you get stuck at this step.

Important! Try to start clean each time (`make clean-all`). Beware, this will **DELETE** all data in your test instances.

## 05: Inspect logs

Open `tilt` for nice web-interface for logs (or any other mean of looking at
progress, be it the bare `docker-compose up` logs on the tty, or attaching to
each container's logs with `ctop` etc).

Normally you would expect all services to be up (green) after a minute or two. The first run can take some time, specially for the nextcloud setup.

## 06: Login into nextcloud to have a look

Login to nextcloud[^nc], it should be listening on `nextcloud.colmena.local:8080` (user: `superadmin`, pass: `superadmin`).
After a few seconds, make sure you see the "Talk" app icon in the top bar.

If you don't see the Talk icon (it looks like a "Q"), go to Tilt and reload the "nextcloud-housekeeping" service.

[^nc]: in a real deployment we don't want nextcloud to be generally accessible, but for now we're more interested in being able to debug and diagnose problems.

## 07: Login to the backend

Open `backend.colmena.local:8080` (user: `admin`, pass: `supersecret` by default). Verify
that you can login and click some stuff.

Verify that in the "Sites" tab you see a few sites that match your configured
domains (`*.colmena.local:8080` by default). If you decided to change the
domain for your deployment, you might need to manually change the sites here too.

## 08: Try to load the colmena web app

Navigate to `colmena.local:8080`. You should see a login page (but don't worry,
you cannot login yet, first we need to create some users, click on invitation links, etc, etc.

## 09: Start the QA for the app

From the backend (`backend.colmena.local:8080` by default), you should first create an organization.

Then, login to the debug mail application (`mail.colmena.local:8080`) and click on the link for the invitation.

After finishing the user registration, you should be able to login again to
`colmena.local:8080` with the newly created user.


## 10: Tear down containers

When you want to stop the services: 

```
make stop
```

You should see all the containers being stopped and removed.

# FAQ 


### 1. Q: I have restarted the backend, but I still see myself logged in in `colmena.local:8080`. Wut?!
A: It's a known bug - just navigate to the toolbar, and logout from the frontend PWA before continue testing.

### 1. Q: But this means that, if I was logged in previously, the invitations will not work
A: Yes. Logout and then click on your invitation link again. We're testing.

# Notes

