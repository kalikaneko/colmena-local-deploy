ASSETS := public/frontend
FRONTEND := colmena-frontend
TILT_PORT ?= 9998
TILT_HOST ?= localhost

ENV_FILE ?= .env
ENV_FILE_BACKEND ?= .env

BACKEND := colmena_backend
NEXTCLOUD := colmena_nextcloud

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif
ifneq (,$(wildcard ${ENV_FILE_BACKEND}))
	include ${ENV_FILE_BACKEND}
    export
endif

# Beware the blinken lights! :)
start: assets
	TILT_PORT=$(TILT_PORT) TILT_HOST=$(TILT_HOST) tilt up

start-local-dev:
	docker-compose --file docker-compose.yml --file docker-compose-local-ports.yml up


rebuild-and-start: assets
	docker-compose up --build

stop:
	docker compose down

assets:
	# build frontend assets and copy them to a cache file
	# that we will pass to caddy later.
	mkdir -p $(ASSETS)
	# there's some level of weirdness in having to pass backend API
	# at the build time. 
	#
	# This basically means that you have to rebuild all the assets if you
	# are deploying colmena on your own instance (that's partly the reason
	# why I'm cheating by reusing the same DNS record, backend.colmena.local,
	# but it also means that we need to keep using the same port).
	#
	# At a later moment (post MVP) I would politely ask
	# the frontend devs to make this configurable at initialization time -
	# it can't be so hard I think.
	cd apps/frontend && docker build -t $(FRONTEND) . 
	# FIXME do not assume tty
	#docker run -v ./$(ASSETS):/assets --rm -it $(FRONTEND) sh -c "cp -r /public/* /assets" 
	sudo chown -R 1000:1000 $(ASSETS)
	docker run -v ./$(ASSETS):/assets --rm $(FRONTEND) sh -c "cp -r /public/* /assets" 

list-nextcloud-apps:
	docker exec -u www-data -it $(NEXTCLOUD) sh -c 'php /var/www/html/occ app:list'

install-nextcloud-apps:
	 docker exec -u www-data -it $(NEXTCLOUD) sh -c 'php /var/www/html/occ app:install spreed'

setup-nextcloud-admin-in-backend:
	docker exec -e SUPERADMIN_PASSWORD_FORCE=1 -it $(BACKEND) sh -c '/opt/app/manage.py create_superadmin $(SUPERADMIN_EMAIL) $(SUPERADMIN_PASSWORD) $(NEXTCLOUD_ADMIN_USER) $(NEXTCLOUD_ADMIN_PASSWORD)'

# Clean & flush all things data
clean-all: clean-assets flush-nc flush-postgres
	docker compose rm

# Delete the cached frontend assets
clean-assets:
	@rm -rf ./public

# This will DELETE all nextcloud data on host disk
flush-nc:
	@sudo chown -R `whoami`:`whoami` data || true
	@rm -rf data

# This will DELETE all postgres data on the nextcloud volume
flush-postgres:
	docker volume rm --force deploy_pg_data || true

.PHONY: assets clean-all
