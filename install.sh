#!/bin/bash
set -e

docker="docker"
tilt="tilt"
repo="https://gitlab.com/kalikaneko/colmena-local-deploy.git"

SERVER_IP="127.0.0.1"

if ! command -v "$docker" &> /dev/null; then
    echo "$docker not found in path, installing";
    # Add Docker's official GPG key:
    curl -fsSL https://get.docker.com -o get-docker.sh;
    sudo sh get-docker.sh;
    sudo usermod -aG docker `whoami`;
    echo "Docker installed. Please logout and login again, and run this script again";
    exit 1;
else
    echo "[+] $docker found in path";
fi

if ! command -v "$tilt" &> /dev/null; then
    echo "$tilt not found in path, installing";
    curl -fsSL https://raw.githubusercontent.com/tilt-dev/tilt/master/scripts/install.sh | bash;
else
    echo "[+] $tilt found in path";
fi

sudo apt-get update && sudo apt install git make

host1="$SERVER_IP    colmena.local"
# Check if the entry already exists in /etc/hosts
if ! grep -qF "$host1" /etc/hosts; then
    echo "$host1" | sudo tee -a /etc/hosts > /dev/null;
    echo "Entry added to /etc/hosts: $host1";
fi
host2="$SERVER_IP    backend.colmena.local"
# Check if the entry already exists in /etc/hosts
if ! grep -qF "$host2" /etc/hosts; then
    echo "$host2" | sudo tee -a /etc/hosts > /dev/null;
    echo "Entry added to /etc/hosts: $host2";
fi
host3="$SERVER_IP    nextcloud.colmena.local"
# Check if the entry already exists in /etc/hosts
if ! grep -qF "$host3" /etc/hosts; then
    echo "$host3" | sudo tee -a /etc/hosts > /dev/null;
    echo "Entry added to /etc/hosts: $host3";
fi
host4="$SERVER_IP    mail.colmena.local"
# Check if the entry already exists in /etc/hosts
if ! grep -qF "$host4" /etc/hosts; then
    echo "$host4" | sudo tee -a /etc/hosts > /dev/null;
    echo "Entry added to /etc/hosts: $host4";
fi

git clone $repo deploy
cd deploy
make start
