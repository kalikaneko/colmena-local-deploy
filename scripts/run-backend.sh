#!/bin/sh
docker run \
    --detach \
    --rm \
    --name colmena_backend \
    --network deploy_colmena_devops \
    -p 5001:5001 \
    --env PORT=5001 \
    --env-file .env.backend \
    docker.io/library/colmena_backend

