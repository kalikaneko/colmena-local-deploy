#!/bin/sh
docker run \
    --rm \
    --name colmena_caddy \
    --network deploy_colmena_devops \
    -p 8080:8080 \
    --add-host colmena.local:127.0.0.1 \
    --add-host mail.colmena.local:127.0.0.1 \
    --add-host backend.colmena.local:127.0.0.1 \
    -v ./public:/srv \
    -v $PWD/Caddyfile.docker:/etc/caddy/Caddyfile \
    caddy:alpine

