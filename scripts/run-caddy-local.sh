#!/bin/bash

# Check if Caddy is already installed, if not, download and install it
if ! command -v caddy &> /dev/null; then
    echo "Caddy is not installed. Downloading and installing..."
    wget -O caddy https://caddyserver.com/api/download?os=linux\&arch=amd64\&idempotency=83615069643988
    chmod +x caddy
fi

# Attempt to use the system-installed Caddy binary
if command -v caddy &> /dev/null; then
    caddy run
else
    # If not found, use a locally available binary
    LOCAL_CADDY="./caddy"  # Assuming the binary is in the same directory as the script
    if [ -x "$LOCAL_CADDY" ]; then
        "$LOCAL_CADDY" run
    else
        echo "Caddy binary not found. Please install Caddy or provide a local binary."
    fi
fi

